//
//  ContentView.swift
//  simnext-virtual-patient
//
//  Created by Audun Indergaard on 18/05/2021.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world!")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
