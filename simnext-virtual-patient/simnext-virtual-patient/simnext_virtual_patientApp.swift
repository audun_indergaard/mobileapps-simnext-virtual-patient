//
//  simnext_virtual_patientApp.swift
//  simnext-virtual-patient
//
//  Created by Audun Indergaard on 18/05/2021.
//

import SwiftUI

@main
struct simnext_virtual_patientApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
